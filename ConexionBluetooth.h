#ifndef ConexionBluetooth_h
#define ConexionBluetooth_h
#include "Arduino.h"

class ConexionBluetooth{

  public: 
    ConexionBluetooth();
    String configureSSID(String msg);
    String configurePass(String msg);

};

#endif
