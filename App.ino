#include <EEPROM.h>
#include "Arduino.h"
#include "ConexionBluetooth.h"
#include "ConexionWifi.h"
#include <SoftwareSerial.h>

//VARIABLES GLOBALES
ConexionBluetooth cbluetooth;
ConexionWifi cwifi;
String ssid;
String pass;
bool configurated;
//CONST
const int RCPin = 14;
const int ledPin = 5;

void setup() {
  configuration();
}

void loop() {

  if(configurated){
    cwifi.doorOpen();
  }else{
    configureNetwork();
  }
  delay(5000);
}

void configuration(){
  Serial.begin(9600);
  pinMode(RCPin , OUTPUT);
  pinMode(ledPin , OUTPUT);
  EEPROM.begin(512);

  ssid = getWifi();
  pass = getPass();

  if(ssid != "" && pass != ""){
    cwifi.setConnection(ssid, pass);
    configurated = true;
  }else{
    configurated = false;
  }
}

void configureNetwork(){

  if (Serial.available() > 0){
    String msg = Serial.readString();    
    String aux = cbluetooth.configureSSID(msg);
    String w, p;
    
    if(aux != ""){
      w = aux;
      setWifi(aux);
    }
    aux = cbluetooth.configurePass(msg);
    if(aux != ""){
      p = aux;
      setPass(aux); 
    }
    cwifi.setConnection(w, p);
    configurated = true;
  }
  
}

void setWifi(String cadena){
  EEPROM.write(0, cadena.length());
  EEPROM.commit();
  
  for(int i=0; i<cadena.length();i++){
    char a=cadena[i];
    EEPROM.write(i + 1, a);
    EEPROM.commit();
  }
}

String getWifi(){
  String res = "";
  int tam = EEPROM.read(0);
 
  for(int i=1; i<=tam;i++){
    char b = EEPROM.read(i);
    res += b;
  }

  return res;
}

void setPass(String cadena){
  EEPROM.write(50, cadena.length());
  EEPROM.commit();

  for(int i=1; i<=cadena.length();i++){
    char a=cadena[i-1];
    EEPROM.write(50 + i, a);
    EEPROM.commit();
  }
}

String getPass(){
  String res = "";
  int tam = EEPROM.read(50);
 
  for(int i=0; i<=tam;i++){
    char b = EEPROM.read(50 + i);
    res += b;

  }

  return res;
}
