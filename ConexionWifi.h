#ifndef ConexionWifi_h
#define ConexionWifi_h
#include "Arduino.h"

class ConexionWifi{

  public: 
    ConexionWifi();
    void setConnection(String wifi, String pass);
    String doorOpen();
    void sendSignal(int res);
    void led(int timeInt);

};

#endif
