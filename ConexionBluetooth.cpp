#include "ConexionBluetooth.h"
#include <SoftwareSerial.h>
#include "Arduino.h"


String msg,cmd;


ConexionBluetooth::ConexionBluetooth(){
  
}


String ConexionBluetooth::configureSSID(String msg){
  String res = "";
  String forReplace = msg.substring(0,msg.indexOf("<>"));
  forReplace.replace("wifi.", "");
  res = forReplace;
  Serial.println("Wifi: " + res);
     
  return res;
}

String ConexionBluetooth::configurePass(String msg){
  String forReplace = msg.substring(0,msg.indexOf("<>"));
  String res = "";  
  msg.replace(forReplace + "<>password.", "");
  res = msg;
  Serial.println("Password: " + res);      
  
  return res;
}
