#include "ConexionWifi.h"
#include <SoftwareSerial.h>
#include "Arduino.h"
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>

String url = "http://51.210.10.167/api/";
//
String ssid1,password;
HTTPClient http;
WiFiClient client;

//CONST
const int RCPin = 14;
const int ledPin = 5;

ConexionWifi::ConexionWifi(){
  
}

void ConexionWifi::setConnection(String wifi, String pass){
  ssid1 = wifi;
  password = pass;
  Serial.println("INTENTANDO CONECTAR: ->" + wifi + "<->" + pass + "<-");

  //CONFIRE NETWORK
  int intentos = 0;
  WiFi.begin("iPhone de Javi", "holamundo");
  //WiFi.begin(wifi, pass);
  
  while (WiFi.status() != WL_CONNECTED && intentos < 1000) {
    delay(1000);
    intentos++;
    Serial.println(intentos);
    led(100);
  }

  if(WiFi.status() != WL_CONNECTED){
    Serial.print("No se ha podido conectar.");
  }else{
    Serial.print(WiFi.localIP());
    for(int i=0; i<5;i++){
      led(500);
    }
  }
  
}

void ConexionWifi::led(int timeInt){
  digitalWrite(ledPin , HIGH);
  delay(timeInt);
  digitalWrite(ledPin , LOW);
}

String ConexionWifi::doorOpen(){
  String response;
  String urlAux = url + "changeData?device=puerta_principal&pass=12345678";
  
  if (http.begin(client, urlAux))
  {
    int httpCode = http.GET();
    if (httpCode > 0) {
        
      if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY) {
        String payload = http.getString();
        Serial.println(payload);
        response = payload;

        if(response == "1"){
          sendSignal(1);
        }if(response == "2"){
          sendSignal(2);
        }else if(response == "3"){
          sendSignal(3);
        }else if(response == "4"){
          sendSignal(4);
        }
  
      }
    }
    else {
      Serial.printf("ERROR: \n", http.errorToString(httpCode).c_str());
    }

    http.end();
  }
  else {
    Serial.printf("ERROR AL CONECTAR\n");
  }

  return response;
}

void ConexionWifi::sendSignal(int res){
  
    digitalWrite(RCPin , HIGH);
    digitalWrite(ledPin , HIGH);
    delay(500);
    digitalWrite(RCPin , LOW);
    digitalWrite(ledPin , LOW);

    Serial.print("Dispositivo ");
    Serial.println(res);
  
}
